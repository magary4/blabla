<?php
/**
 * Created by Sascha Ormanns - diePartments
 * 01.02.18 09:37
 */
namespace ix\Bundle\TintometerBundle\Document;

class DataSheet
{
    /** @var string */
    private $title;

    /** @var string */
    private $orderNumber;

    /** @var array */
    private $documentUrls;

    /** @var  array */
    private $availableCountryCodes;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return DataSheet
     */
    public function setTitle(string $title): DataSheet
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return DataSheet
     */
    public function setOrderNumber(string $orderNumber): DataSheet
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @param string $countryCode
     * @return string
     */
    public function getDocumentUrlByCountryCode(string $countryCode): string
    {
        if (!isset($this->documentUrls[$countryCode])) {
            return '';
        }

        return $this->documentUrls[$countryCode];
    }

    /**
     * @return array
     */
    public function getDocumentUrls(): array
    {
        return $this->documentUrls;
    }

    /**
     * @param array $documentUrls
     * @return DataSheet
     */
    public function setDocumentUrls(array $documentUrls): DataSheet
    {
        $this->documentUrls = $documentUrls;
        return $this;
    }

    /**
     * @return array
     */
    public function getAvailableCountryCodes(): array
    {
        return $this->availableCountryCodes;
    }

    /**
     * @param array $availableCountryCodes
     * @return DataSheet
     */
    public function setAvailableCountryCodes(array $availableCountryCodes): DataSheet
    {
        $this->availableCountryCodes = $availableCountryCodes;
        return $this;
    }
}
